import javax.swing.SwingUtilities;

public class SFGmain {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new SFGview();
			}
		});
	}

}
