import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.jgrapht.Graph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.ListenableDirectedWeightedGraph;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;

public class SFGview {

	private SFGcontroller controller;
	private JFrame frame;
	private SFGview view;

	public SFGview() {

		createAndShowGui();

		view = this;
		controller = new SFGcontroller(view);

	}

	private void createAndShowGui() {

		frame = new JFrame("SFG Application");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton addVertex, calculateGain, removeVertex, addSelfLoop,removeGraph;

		addVertex = new JButton("Add Vertex");
		calculateGain = new JButton("Calculate Gain");
		removeVertex = new JButton("Remove Vertex");
		addSelfLoop = new JButton("Add SelfLoop");
		removeGraph = new JButton("Remove Graph");


		calculateGain.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.calculateGain();

			}
		});

		removeVertex.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					int nodeNum = Integer
							.valueOf(JOptionPane.showInputDialog("Please, Enter number of The node you want to remove"));
					controller.removeVertex(nodeNum);
					frame.repaint();
				} catch (Exception ex) {
					ex.getMessage();
					JOptionPane.showMessageDialog(frame, "Error!! an Integer should be entered !!");
				}
			}
		});
		
		removeGraph.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				SFGmain.main(null);
			}
		});

		addVertex.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.addVertex();
				frame.repaint();
			}
		});
		
		addSelfLoop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {
					int nodeNum = Integer
							.valueOf(JOptionPane.showInputDialog("Please, Enter number of The node you want to add selfloop to"));
					controller.addSelfLoop(nodeNum);
					frame.repaint();
				} catch (Exception ex) {
					ex.getMessage();
					JOptionPane.showMessageDialog(frame, "Error!! an Integer should be entered !!");

				}
			}
		});

		// frame.pack();
		// frame.setLocationByPlatform(true);
		frame.setVisible(true);


		calculateGain.setBounds(15, 530, 125, 25);
		addSelfLoop.setBounds(155, 530, 125, 25);
		removeGraph.setBounds(333,530,124,25);
		addVertex.setBounds(510, 530, 125, 25);
		removeVertex.setBounds(650, 530, 125, 25);
		
		frame.add(calculateGain);
		frame.add(addSelfLoop);
		frame.add(removeGraph);
		frame.add(addVertex);
		frame.add(removeVertex);
		
		frame.setBounds(100, 100, 800, 600);
		frame.setResizable(false);
	}

	public JFrame getFrame() {
		return frame;
	}

}