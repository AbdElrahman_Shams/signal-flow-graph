import org.jgrapht.graph.DefaultWeightedEdge;

@SuppressWarnings("serial")
public class MyEdge extends DefaultWeightedEdge {
	
	@Override
	public String toString() {
		return String.valueOf(getWeight());
	}
	
	@Override
	public double getWeight() {
		return super.getWeight();
	}
	
}