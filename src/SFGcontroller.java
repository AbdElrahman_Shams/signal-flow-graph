import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.ListenableDirectedWeightedGraph;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;

@SuppressWarnings("deprecation")
public class SFGcontroller {
	
	
	private int i ;
	private ListenableGraph<String, MyEdge> g;
	private SFGview view;
	private SFGmodel model;
	private ArrayList<Boolean> removedVertices;
	private mxGraphComponent graphComponent;
 	
	public SFGcontroller(SFGview view) {
		this.view = view;
		
		i = 0;
		g = buildGraph();
		prepareFrame();
		removedVertices = new ArrayList<Boolean>();
	}
	
	private void prepareFrame() {
		
		JGraphXAdapter<String, MyEdge> graphAdapter = new JGraphXAdapter<String, MyEdge>(g);
		mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
		layout.execute(graphAdapter.getDefaultParent());
		graphComponent = new mxGraphComponent(graphAdapter);
		view.getFrame().add(graphComponent);
	}
	

	private ListenableGraph<String, MyEdge> buildGraph() {
		@SuppressWarnings("deprecation")
		ListenableDirectedWeightedGraph<String, MyEdge> g = new ListenableDirectedWeightedGraph<String, MyEdge>(
				MyEdge.class);
		
		return g;
	}
	
	public void addVertex() {
		
		++i;
		
		removedVertices.add(false);
		
		String verName = "Node" + String.valueOf(i); 
		g.addVertex(verName);
			
	}
	
	public void removeVertex(int nodeNum) {
		
		if (nodeNum > i) {
			JOptionPane.showMessageDialog(view.getFrame(), "The node you want doesn`t exist !!");
		}else if (removedVertices.get(nodeNum-1)) {
			JOptionPane.showMessageDialog(view.getFrame(), "The node you want doesn`t exist !!");
		} else {
			removedVertices.set(nodeNum-1, true);
			String name = "Node" + String.valueOf(nodeNum);
			g.removeVertex(name);
		}
		
		
	}
	
	public void addSelfLoop(int nodeNum) {
		
		if (nodeNum > i) {
			JOptionPane.showMessageDialog(view.getFrame(), "The node you want doesn`t exist !!");
		}else if (removedVertices.get(nodeNum-1)) {
			JOptionPane.showMessageDialog(view.getFrame(), "The node you want doesn`t exist !!");
		} else {
			String name = "Node" + String.valueOf(nodeNum);
			g.addEdge(name, name);
		}
		
	} 
	
	public void calculateGain() {
		
		model = new SFGmodel(g);
		
		JOptionPane.showMessageDialog(view.getFrame(), "OverAll Gain = "  + model.calculateOverAllGain());
		
	}

}
